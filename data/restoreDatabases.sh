#!/bin/bash

# groep8-prd keeps its data
for db in groep8-dev groep8-tst groep8-acc
do
    echo "Dropping $db"
    mongo $db --eval "db.dropDatabase()"
    echo "Restoring $db"
    mongorestore -d $db seed
done