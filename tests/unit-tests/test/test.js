// Load configuration
var env = process.env.NODE_ENV || 'development',
    config = require('../../../server/config/config.js')[env],
    localConfig = require('../../config-test.json')
    ;

var should = require('should'),
    supertest = require('supertest');

describe('API Routing for CRUD operations on users', function () {
    var request = supertest(localConfig.host + ":" + config.port + "/" + localConfig.api_path);

    var tmpUserId = null;
    var tmpUserResponse;

    before(function (done) {
        done();
    });

    describe('CREATE user', function () {
        console.log('Creating user \n');
        it('Should POST /users', function (done) {
            request
                .post('/users')
                .send({
                    "firstName": "Koen",
                    "lastName": "Barmentlo",
                    "facebookId": '151345134363',
                    "email": "koen@barmentlo.nl",
                    "street": "straat",
                    "number": "24",
                    "city": "Arnhem",
                    "postalCode": "8585LL"
                }
            )
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('create');
                    JSON.parse(res.text)
                        .should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    res.type.should.be.exactly('application/json');
                    res.charset.should.be.exactly('utf-8');
                    tmpUserId = JSON.parse(res.text).doc._id;
                    console.log(res.text);
                    done();
                });
        });
    });

    describe('RETRIEVE all users', function () {
        console.log('Retrieving all users \n');
        it('Should GET /users', function (done) {
            request
                .get('/users')
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);

                    tmpUserResponse = res.text;

                    done();
                });
        });
    });

    describe('RETRIEVE 1 user', function () {
        console.log("Retrieving one user \n")
        it('Should GET /users/{id}', function (done) {
            request
                .get('/users/' + tmpUserId)
                //.expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action')
                        .be.exactly('detail');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('firstName')
                        .be.exactly('Koen');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('lastName')
                        .be.exactly('Barmentlo');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('email')
                        .be.exactly('koen@barmentlo.nl');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('street')
                        .be.exactly('straat');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('number')
                        .be.exactly('24');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('city')
                        .be.exactly('Arnhem');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('postalCode')
                        .be.exactly('8585LL');
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('UPDATE 1 user', function () {
        console.log('Updating user \n');
        it('Should PUT /users/{id}', function (done) {
            request
                .put('/users/' + tmpUserId)
                .send({
                    "firstName": "Ben",
                    "lastName": "Stouwe",
                    "email": "ben@stouwe.nl",
                    "street": "wegstraat",
                    "number": "26",
                    "city": "Zwolle",
                    "postalCode": "8585BN",
                })
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action')
                        .be.exactly('update');
                    JSON.parse(res.text)
                        .should.have.property('err')
                        .be.exactly(null);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('firstName')
                        .be.exactly('Ben');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('lastName')
                        .be.exactly('Stouwe');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('email')
                        .be.exactly('ben@stouwe.nl');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('street')
                        .be.exactly('wegstraat');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('number')
                        .be.exactly('26');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('city')
                        .be.exactly('Zwolle');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('postalCode')
                        .be.exactly('8585BN');
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('DELETE 1 user', function () {
        console.log('Deleting user \n');
        it('Should DELETE /users/{id}', function (done) {
            request
                .del('/users/' + tmpUserId)
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('delete');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('ok')
                        .be.exactly(1);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('n')
                        .be.exactly(1);
                    JSON.parse(res.text).should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('RETRIEVE all users to verify that the original collection is restored.', function () {
        console.log('Retrieving all users to verify that the original collection is restored. \n');
        it('Should GET /users', function (done) {
            request
                .get('/users')
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);

                    done();
                });
        });
    });
});

/** Guitar Parts */

describe('API Routing for CRUD operations on guitar parts', function () {
    var request = supertest(localConfig.host + ":" + config.port + "/" + localConfig.api_path);

    var tmpUserId = null;
    var tmpUserResponse;
    var tmpGuitarPartId = null;

    before(function (done) {
        done();
    });

    describe('CREATE guitarPart', function () {
        console.log('Creating guitarPart \n');
        it('Should POST /guitarParts', function (done) {
            request
                .post('/guitarParts')
                .send({
                    "type": "body",
                    "brand": "gibson",
                    "model": "t-1000",
                    "price": "45.00",
                    "image": "gibsont-1000.png",
                    "icon": "gibsont-1000.gif",
                    "genres": ["rock"],
                    "availableMaterials": ["wood"],
                    "availableColors": ["red", "blue"]
                }
            )
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('create');
                    JSON.parse(res.text)
                        .should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    res.type.should.be.exactly('application/json');
                    res.charset.should.be.exactly('utf-8');
                    tmpGuitarPartId = JSON.parse(res.text).doc._id;
                    console.log(res.text);
                    done();
                });
        });
    });

    describe('RETRIEVE all guitarParts', function () {
        console.log('Retrieving all guitar parts \n');
        it('Should GET /guitarParts', function (done) {
            request
                .get('/guitarParts')
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);

                    tmpUserResponse = res.text;

                    done();
                });
        });
    });

    describe('RETRIEVE 1 guitarPart', function () {
        console.log("Retrieving one guitarPart \n")
        it('Should GET /guitarParts/{id}', function (done) {
            request
                .get('/guitarParts/' + tmpGuitarPartId)
                //.expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action')
                        .be.exactly('detail');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('type')
                        .be.exactly('body');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('brand')
                        .be.exactly('gibson');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('model')
                        .be.exactly('t-1000');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('price')
                        .be.exactly(45);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('image')
                        .be.exactly('gibsont-1000.png');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('icon')
                        .be.exactly('gibsont-1000.gif');
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('genres')
                    //    .be.members(['rock']);
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('availableMaterials')
                    //    .be.exactly(['wood']);
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('availableColors')
                    //    .be.exactly(['red', 'blue']);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('UPDATE 1 guitarPart', function () {
        console.log('Updating guitar part \n');
        it('Should PUT /guitarParts/{id}', function (done) {
            request
                .put('/guitarParts/' + tmpGuitarPartId)
                .send({
                    "type": "head",
                    "brand": "fender",
                    "model": "f-1000",
                    "price": "50",
                    "image": "fenderf-1000.png",
                    "icon": "fenderf-1000.gif",
                    "genres": ["blues"],
                    "availableMaterials": ["plastic"],
                    "availableColors": ["green", "yellow"]
                })
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action')
                        .be.exactly('update');
                    JSON.parse(res.text)
                        .should.have.property('err')
                        .be.exactly(null);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('type')
                        .be.exactly('head');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('brand')
                        .be.exactly('fender');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('model')
                        .be.exactly('f-1000');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('price')
                        .be.exactly("50");
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('image')
                        .be.exactly('fenderf-1000.png');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('icon')
                        .be.exactly('fenderf-1000.gif');
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('genres')
                    //    .be.exactly(['blues']);
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('availableMaterials')
                    //    .be.exactly(['plastic']);
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('availableColors')
                    //    .be.exactly(['green', 'yellow']);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('DELETE 1 guitarPart', function () {
        console.log('Deleting guitarPart \n');
        it('Should DELETE /guitarParts/{id}', function (done) {
            request
                .del('/guitarParts/' + tmpGuitarPartId)
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('delete');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('ok')
                        .be.exactly(1);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('n')
                        .be.exactly(1);
                    JSON.parse(res.text).should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('RETRIEVE all guitarParts to verify that the original collection is restored.', function () {
        console.log('Retrieving all guitarParts to verify that the original collection is restored. \n');
        it('Should GET /guitarParts', function (done) {
            request
                .get('/guitarParts')
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);

                    done();
                });
        });
    });

});

/** Guitars */
/** TODO NEEDS FIXING */

describe('API Routing for CRUD operations on guitars', function () {
    var request = supertest(localConfig.host + ":" + config.port + "/" + localConfig.api_path);

    var tmpUserId = null;
    var tmpUserResponse;
    var tmpGuitarId = null;

    before(function (done) {
        done();
    });

    describe('CREATE guitar', function () {
        console.log('Creating guitar \n');
        it('Should POST /guitars', function (done) {
            request
                .post('/guitars')
                .send({
                    "name": "superGitaar",
                    "type": "left",
                    "userid": "5",
                    "likes": "30",
                    "dislikes": "2",
                    "guitarparts": []
                }
            )
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('create');
                    JSON.parse(res.text)
                        .should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    res.type.should.be.exactly('application/json');
                    res.charset.should.be.exactly('utf-8');
                    tmpGuitarId = JSON.parse(res.text).doc._id;
                    console.log(res.text);
                    done();
                });
        });
    });

    describe('RETRIEVE all guitars', function () {
        console.log('Retrieving all guitar \n');
        it('Should GET /guitars', function (done) {
            request
                .get('/guitars')
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);

                    tmpUserResponse = res.text;

                    done();
                });
        });
    });

    describe('RETRIEVE 1 guitar', function () {
        console.log("Retrieving one guitar \n")
        it('Should GET /guitars/{id}', function (done) {
            request
                .get('/guitars/' + tmpGuitarId)
                //.expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action')
                        .be.exactly('detail');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('name')
                        .be.exactly('superGitaar');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('type')
                        .be.exactly('left');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('userid')
                        .be.exactly('5');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('likes')
                        .be.exactly(30);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('dislikes')
                        .be.exactly(2);

                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('guitarparts')
                    //    .be.members([]);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('UPDATE 1 guitar', function () {
        console.log('Updating guitar \n');
        it('Should PUT /guitars/{id}', function (done) {
            request
                .put('/guitars/' + tmpGuitarId)
                .send({
                    "name": "superGitaar2",
                    "type": "normal",
                    "userid": "5",
                    "likes": "22",
                    "dislikes": "11",
                    "guitarparts": []
                })
                .expect(200)                                                  // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action')
                        .be.exactly('update');
                    JSON.parse(res.text)
                        .should.have.property('err')
                        .be.exactly(null);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('name')
                        .be.exactly('superGitaar2');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('type')
                        .be.exactly('normal');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('userid')
                        .be.exactly("5");
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('likes')
                        .be.exactly("22");
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('dislikes')
                        .be.exactly("11");
                    //JSON.parse(res.text)
                    //    .should.have.property('doc')
                    //    .and.have.property('guitarparts')
                    //    .be.exactly([]);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('DELETE 1 guitar', function () {
        console.log('Deleting guitar \n');
        it('Should DELETE /guitars/{id}', function (done) {
            request
                .del('/guitars/' + tmpGuitarId)
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('delete');
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('ok')
                        .be.exactly(1);
                    JSON.parse(res.text)
                        .should.have.property('doc')
                        .and.have.property('n')
                        .be.exactly(1);
                    JSON.parse(res.text).should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    describe('RETRIEVE all guitars to verify that the original collection is restored.', function () {
        console.log('Retrieving all guitars to verify that the original collection is restored. \n');
        it('Should GET /guitars', function (done) {
            request
                .get('/guitars')
                .expect(200)                                                // supertest
                //.expect('Content-Type', /application.json/)                 // supertest
                //.expect('Content-Type', 'utf-8')                            // supertest
                .end(function (err, res) {
                    if (err) {
                        //console.log(err);
                        throw err;
                    }

                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);

                    done();
                });
        });
    });

});