/**
 * Created by manue_000 on 5/26/2015.
 */
/*jslint node:true */
"use strict";

/** @module Routes for guitars */
/** @class */
var express = require('express');
var router = express.Router();

/**  user routes
 ---------------
 We create a variable "user" that holds the controller object.
 We map the URL to a method in the created variable "controller".
 In this example is a mapping for every CRUD action.
 */
var controller = require('../app/controllers/guitars.js');

/** CREATE route for guitars */
router
    .post('/guitars', controller.create);

// RETRIEVE
router
    .get('/guitars', controller.list)
    .get('/guitars/:_id', controller.detail);

// UPDATE
router
    .put('/guitars/:_id', controller.updateOne);

// DELETE
router
    .delete('/guitars/:_id', controller.deleteOne);


module.exports = router;
