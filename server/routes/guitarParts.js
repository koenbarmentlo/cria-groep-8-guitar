/*jslint node:true */


/** @module Routes for guitar parts */
/** @class */
var express = require('express');
var router = express.Router();

/**  guitar part routes
 ---------------
 We create a variable "user" that holds the controller object.
 We map the URL to a method in the created variable "controller".
 In this example is a mapping for every CRUD action.
 */
var controller = require('../app/controllers/guitarParts.js');

/** CREATE route for guitar part */
router
    .post('/guitarParts', controller.create);

// RETRIEVE
router
    .get('/guitarParts', controller.list)
    .get('/guitarParts/:_id', controller.detail);

// UPDATE
router
    .put('/guitarParts/:_id', controller.updateOne);

// DELETE
router
    .delete('/guitarParts/:_id', controller.deleteOne);


module.exports = router;