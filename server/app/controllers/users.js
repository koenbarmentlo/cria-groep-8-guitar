/*jslint node: true */
"use strict";

var mongoose = require('mongoose'),
    User = mongoose.model('user');

exports.create = function (req, res) {

    var doc = new User(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {
                "action": "create",
                'timestamp': new Date(),
                filename: __filename
            },
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};

exports.list = function (req, res) {
    var conditions, fields, sort;

    conditions = {};
    fields = {};
    sort = { 'modificationDate': -1 };

    User
        .find(conditions, fields)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {
                    "action": "list",
                    'timestamp': new Date(),
                    filename: __filename
                },
                doc: doc, // array
                err: err
            };
            return res.send(retObj);
        });
};

exports.detail = function (req, res) {
    var conditions, fields;

    conditions = { _id: req.params._id };
    fields = {};
    User
        .findOne(conditions, fields)
        .exec(function (err, doc) {
            var retObj = {
                meta: { "action": "detail", 'timestamp': new Date(), filename: __filename },
                doc: doc, // only the first document, not an array when using "findOne"
                err: err
            };
            console.log(retObj);
            return res.send(retObj);
        });
};

exports.updateOne = function (req, res) {

    var conditions =
        { _id: req.params._id },
        update = {
            firstName: req.body.firstName || '',
            lastName: req.body.lastName || '',
            facebookId: req.body.facebookId || '',
            email: req.body.email || '',
            street: req.body.street || '',
            number: req.body.number || '',
            city: req.body.city || '',
            postalCode: req.body.postalCode || ''
        },
        options = { multi: false },
        callback = function (err, doc) {
            var retObj = {
                meta: {
                    "action": "update",
                    'timestamp': new Date(),
                    filename: __filename,
                    'doc': doc,
                    'update': update
                },
                doc: update,
                err: err
            };
            return res.send(retObj);
        };

    User
        .findOneAndUpdate(conditions, update, options, callback);
};

exports.deleteOne = function (req, res) {
    var conditions, callback, retObj;

    conditions = { _id: req.params._id };
    callback = function (err, doc) {
        retObj = {
            meta: {
                "action": "delete",
                'timestamp': new Date(),
                filename: __filename
            },
            doc: doc,
            err: err
        };
        return res.send(retObj);
    };

    User
        .remove(conditions, callback);
};