/*jslint node:true */

(function () {
    "use strict";
    /**
     * Module dependencies.
     */
    var mongoose = require('mongoose'),
        Schema = mongoose.Schema,
        guitarSchema,
        guitarDefinition,
        modelName;

    /**
     * Creates a new mongoose schema for the guitars collection.
     */
    guitarDefinition = {
        _id: { type: String, required: false },
        color: { type: String, required: false },
        material: { type: String, required: false },
        x: Number,
        y: Number,
        width: Number,
        heigth: Number
    };

    guitarSchema = new Schema({
        name: { type: String, required: false, unique: false },
        type: { type: String, required: false },
        userid: String,
        likes: Number,
        dislikes: Number,
        kidsguitar: Boolean,
        lefthanded: Boolean,
        assemblykit: Boolean,

        guitarParts: {
            body: guitarDefinition,
            neck: guitarDefinition,
            head: guitarDefinition,
            bridge: guitarDefinition,
            frets: guitarDefinition,
            pickup: guitarDefinition,
            buttons: guitarDefinition
        }
    },
           { collection: 'guitars' });

    /**
     * Custom validator
     */
    //NOT YET IMPLEMENTED
    /*guitar.path('title').validate(function (val) {
        return (val !== undefined && val !== null && val.length >= 8);
    }, 'Invalid title');*/

    modelName = "guitarSchema";
    mongoose.model(modelName, guitarSchema);

}());