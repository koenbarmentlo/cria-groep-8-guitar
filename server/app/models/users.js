/*jslint node:true */

(function () {
    "use strict";
    /**
     * Module dependencies.
     */
    var mongoose = require('mongoose'),
        Schema = mongoose.Schema,
        user,
        modelName;

    /**
     * Creates a new mongoose schema for the users collection.
     */
    user = new Schema({
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        facebookId: {type: String, required: false },
        email: { type: String, required: true, unique: true },
        street: { type: String, required: false },
        number: { type: String, required: false },
        city: { type: String, required: false },
        postalCode: { type: String, required: false }
    },
        { collection: 'users' });

    /**
     * Custom validator
     * @returns true or false. In case of ```false```, a message 'Invalid title' is returned as well.
     * @see http://mongoosejs.com/docs/validation.html
     */
    //NOT YET IMPLEMENTED
    /*user.path('title').validate(function (val) {
        return (val !== undefined && val !== null && val.length >= 8);
    }, 'Invalid title');*/

    modelName = "user";
    mongoose.model(modelName, user);

}());