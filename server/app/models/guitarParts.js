/*jslint node:true */

(function () {
    "use strict";
    /**
     * Module dependencies.
     */
    var mongoose = require('mongoose'),
        Schema = mongoose.Schema,
        guitarPart,
        modelName;

    /**
     * Creates a new mongoose schema for the parts collection.
     * @class Schema/guitarPart
     * @returns Schema object
     * @see http://www.json.org/
     * @see http://mongoosejs.com/docs/schematypes.html
     * @see http://mongoosejs.com/docs/guide.html#collection
     */
    guitarPart = new Schema({
        type: { type: String, required: true },
        brand: { type: String, required: true},
        model: { type: String, required: true},
        price: { type: Number, required: true },
        image: { type: String, required: true },
        icon: { type: String, required: true },
        genres: [String],
        availableMaterials: [String],
        availableColors: [String]
    },
        { collection: 'guitarParts' });

    /**
     * Custom validator
     */
    //NOT YET IMPLEMENTED
    /*guitarPart.path('title').validate(function (val) {
        return (val !== undefined && val !== null && val.length >= 8);
    }, 'Invalid title');*/

    modelName = "guitarPart";
    mongoose.model(modelName, guitarPart);

}());