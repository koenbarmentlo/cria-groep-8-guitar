/*jslint node: true */
/*globals guitarDesignApp, homepage */

guitarDesignApp.controller('HomeController', ['$scope', 'GuitarsService', 'Facebook', '$interval', function ($scope, GuitarsService, Facebook, $interval) {
    "use strict";
    homepage.currentTarget = "#intro";
    homepage.animateTo(homepage.currentTarget);
    homepage.addScrollListener();
    homepage.setNavbarBackground(homepage.currentTarget);
    homepage.setSelectedClass(homepage.currentTarget);

    $scope.animateTo = homepage.animateTo;

    /* Genres */
    $scope.genres = [
        {
            name: 'Country',
            image: '../content/img/country.png'
        },
        {
            name: 'Rock',
            image: '../content/img/rock.png'
        },
        {
            name: 'Metal',
            image: '../content/img/metal.png'
        },
        {
            name: 'Jazz',
            image: '../content/img/jazz.png'
        },
        {
            name: 'Reggae',
            image: '../content/img/reggae.png'
        },
        {
            name: 'Pop',
            image: '../content/img/pop.png'
        }];

    $scope.selectedGenre = null;

    $scope.selectGenre = function (genreName) {
        var genre;
        if (genreName === $scope.selectedGenre) {
            $scope.selectedGenre = null;
            document.querySelector("#" + genreName).classList.remove('selected');
        } else {
            for (genre in $scope.genres) {
                if ($scope.genres.hasOwnProperty(genre)) {
                    document.querySelector("#" + $scope.genres[genre].name).classList.remove('selected');
                    if (genreName === $scope.genres[genre].name) {
                        $scope.selectedGenre = genreName;
                        document.querySelector("#" + genreName).classList.add('selected');
                    }
                }
            }
        }
    };

    /* Facebook settings and functions*/
    $scope.loginStatus = 'disconnected';
    $scope.facebookIsReady = false;
    $scope.user = {};
    $scope.user.first_name = 'Login';
    $scope.login = homepage.login;

    $scope.removeAuth = homepage.removeAuth;

    $scope.api = homepage.api;
    $scope.api = function () {
        Facebook.api('/me', function (response) {
            //var tempUsr;
            $scope.user.facebookId = response.id;
            //tempUsr = GuitarsService.users.query({ facebookId : response.id });
            //console.log(tempUsr);
            //if (tempUsr.facebookId === $scope.user.facebookId) {
            //
            //}
            $scope.user.firstName = response.first_name;
            $scope.user.lastName = response.last_name;
            $scope.user.email = response.email;
            GuitarsService.users.save({}, $scope.user, function (res) {
                console.log("user saved");
            });
        });
    };

    $scope.$watch(function () {
        return Facebook.isReady();
    },
        function (newVal) {
            if (newVal) {
                $scope.facebookIsReady = true;
                Facebook.getLoginStatus(function (response) {
                    $scope.loginStatus = response.status;
                    if ($scope.loginStatus === 'connected') {
                        $scope.api();
                    }
                });
            }
        }
        );

    $scope.inspirations = [
        {
            name: 'Lemmy Kilmister',
            band: 'Motorhead',
            image: '../content/img/inspiration/lemmy.jpg',
            size: 'smallPhoto'
        },
        {
            name: 'Shagrath',
            band: 'Dimmu Borgir',
            image: '../content/img/inspiration/shagrath-dimmu.png',
            size: 'smallPhoto'
        },
        {
            name: 'Paul McCartney',
            band: 'The Beatles',
            image: '../content/img/inspiration/Paul_McCartney_black_and_white_2010.jpg',
            size: 'bigPhoto'
        },
        {
            name: 'Tim Armstrong',
            band: 'Rancid',
            image: '../content/img/inspiration/timarmstrong-rancid.jpg',
            size: 'bigPhoto'
        },
        {
            name: 'Courtney Love',
            band: 'Hole',
            image: '../content/img/inspiration/Courtney-Love-014.jpg',
            size: 'smallPhoto'
        },
        {
            name: 'Zakk Wylde',
            band: 'Black Sabbath',
            image: '../content/img/inspiration/zakkwylde.jpg',
            size: 'smallPhoto'
        }];



    $scope.selectInspiration = function (selected) {
        //if (!$scope.overlay) {
        $scope.selectedInspiration = selected;
        console.log($scope.selectedInspiration);
        //}
    };

    $scope.changeBackgroundOnInterval = function () {
        var backgrounds,
            currentBackgroundIndex;
        backgrounds = ["background1", "background2", "background3"];
        $scope.background = backgrounds[0];
        currentBackgroundIndex = backgrounds.indexOf($scope.background);
        $interval(function () {
            currentBackgroundIndex += 1;
            if (currentBackgroundIndex >= backgrounds.length) {
                currentBackgroundIndex = 0;
            }
            $scope.background = backgrounds[currentBackgroundIndex];
        }, 5000);
    };
    $scope.changeBackgroundOnInterval();

    $scope.overlay = false;
    /* TEST FOR OVERLAY */
    $scope.openDetail = function () {
        if (!$scope.overlay) {
            console.log("open");
            $scope.overlay = true;
            document.querySelector(".overlay").style.visibility = "visible";
            document.querySelector(".overlay").style.opacity = 1;
            document.querySelector(".overlay").style.zIndex = 999;
        }
    };

    $scope.closeDetail = function () {
        if ($scope.overlay) {
            console.log("close");
            $scope.overlay = false;
            document.querySelector(".overlay").style.opacity = 0;
            document.querySelector(".overlay").style.visibility = "hidden";
            document.querySelector(".overlay").style.zIndex = 0;
        }
    };
}]);
