/*globals homepage, $*/
/*jslint browser: true*/
/*globals FB, $scope, Facebook */


(function () {
    "use strict";
    window.homepage = {

        currentTarget: "#intro",

        addScrollListener: function () {
            window.addEventListener('wheel', function (e) {
                var newHash,
                    mouseWheelDown;
                e.preventDefault();
                mouseWheelDown = e.deltaY > 0;
                newHash = homepage.getNewHash(homepage.currentTarget, mouseWheelDown);
                homepage.animateTo(newHash);
            }, false);
        },

        animateTo: function (target) {
            var $target;
            $target = $(target);
            homepage.setSelectedClass(target);
            homepage.setNavbarBackground(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
                $target[0].scrollIntoView(true);
                homepage.currentTarget = target;
            });
        },

        setNavbarBackground: function (target) {
            var navbar = document.getElementById('topNavBar');
            if (target === "#intro" || target === "") {
                navbar.classList.add("transparent");
            } else {
                navbar.classList.remove("transparent");
            }
        },

        setSelectedClass: function (target) {
            target = target.replace("#", "");
            homepage.markNavbarItemAsSelected(target);
            homepage.markNavigationDotAsSelected(target);

        },

        markNavbarItemAsSelected: function (target) {
            var selectedNavbarItem,
                navbaritems,
                i;
            navbaritems = document.querySelectorAll("#navbaritems > li");
            for (i = 0; i < navbaritems.length; i += 1) {
                navbaritems[i].classList.remove("selected");
            }
            selectedNavbarItem = document.querySelectorAll('#navbaritems > li.' + target)[0];
            selectedNavbarItem.classList.add("selected");
        },

        markNavigationDotAsSelected: function (target) {
            var dots,
                selectedDot,
                i;
            dots = document.querySelectorAll("#navigationdots > li");
            for (i = 0; i < dots.length; i += 1) {
                dots[i].classList.remove("selected");
            }
            selectedDot = document.querySelectorAll('#navigationdots > li.' + target)[0];
            if (selectedDot !== undefined) {
                selectedDot.classList.add("selected");
            }
        },

        getNewHash: function (hash, next) {
            var targets,
                index;
            targets = ["#intro", "#genre", "#builder", "#inspiration"];
            index = targets.indexOf(hash);
            if (index > 0 && !next) {
                return targets[index - 1];
            }
            if (index < targets.length - 1 && next) {
                return targets[index + 1];
            }
            return targets[index];
        },

        /*FACEBOOK*/
        login: function () {
            Facebook.login(function (response) {
                $scope.loginStatus = response.status;
                if ($scope.loginStatus === 'connected') {
                    $scope.api();
                } else {
                    $scope.user = [];
                    $scope.user.first_name = 'Login';
                }
            });
        },

        removeAuth : function () {
            Facebook.api({
                method: 'Auth.revokeAuthorization'
            }, function (response) {
                Facebook.getLoginStatus(function (response) {
                    $scope.loginStatus = response.status;
                    if ($scope.loginStatus === 'connected') {
                        $scope.api();
                    }
                });
            });
        },

        api: function () {
            Facebook.api('/me', function (response) {
                $scope.user = response;
            });
        },

        shareGuitar: function (url) {
            FB.ui({
                method: 'share',
                link: url,
                href: 'http://server3.tezzt.nl:6075/#/builder',
                caption: 'An example caption',
            }, null); /* null = function with one parameter: response*/
        }

    };
}());