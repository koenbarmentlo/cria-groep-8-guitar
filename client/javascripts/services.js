/*global angular, console, FB, $ */

(function () {
    "use strict";
    angular.module('guitarDesignApp.services', ['ngResource'])
        .factory('GuitarsService', ['$resource', '$http', function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'query': {method: 'GET', isArray: true},
                    'update': {method: 'PUT'},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            // REST url to server
            db.users = $resource('/api/users/:_id', {}, actions);
            db.guitarParts = $resource('/api/guitarParts/:_id', {}, actions);
            db.guitars = $resource('/api/guitars/:_id', {}, actions);
            return db;
        }]);
}());
