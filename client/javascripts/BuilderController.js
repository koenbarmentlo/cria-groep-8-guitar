/*jslint node: true */
/*jslint bitwise: true */
/*globals guitarDesignApp, homepage, Facebook */

guitarDesignApp.controller('BuilderController', ['$scope', 'GuitarsService', function ($scope, GuitarsService) {
    "use strict";
    $scope.animateTo = function (e) {
        window.location = "/";
    };

    $scope.modifyGuitarPart = function (id) {
        var part;
        //body
        part = $scope.getGuitarPartById(id);
        $scope.guitar.guitarParts[$scope.selectedPartType]._id = part._id;
        $scope[$scope.selectedPartType] = part.image;
        //color
        $scope.drawGuitar();
        $scope.calculatePrice();
    };

    $scope.drawGuitar = function () {
        var bodyCanvas,
            context,
            bodyImage,
            guitarPart,
            guitarPartColor;
        bodyCanvas = document.getElementById("cnvs" + $scope.selectedPartType);
        bodyImage = new Image();
        bodyImage.src = $scope[$scope.selectedPartType];
        context = bodyCanvas.getContext('2d');
        context.clearRect(0, 0, bodyCanvas.width, bodyCanvas.height);
        context.drawImage(bodyImage, 0, 0, bodyCanvas.width, bodyCanvas.height);

        guitarPart = $scope.guitar.guitarParts[$scope.selectedPartType];
        if (guitarPart.color !== undefined) {
            guitarPartColor = $scope.hexToRGB(guitarPart.color);
        } else {
            guitarPartColor = $scope.hexToRGB("#000");
        }
        $scope.changeColor(bodyCanvas, guitarPartColor);
    };

    $scope.changeColor = function (canvas, rgbColorCode) {
        var context,
            pixels,
            i;
        context = canvas.getContext('2d');
        pixels = context.getImageData(0, 0, canvas.width, canvas.height);
        for (i = 0; i < pixels.data.length; i += 4) {
            pixels.data[i] = rgbColorCode.R;
            pixels.data[i + 1] = rgbColorCode.G;
            pixels.data[i + 2] = rgbColorCode.B;
        }
        context.putImageData(pixels, 0, 0);
    };

    $scope.getGuitarPartById = function (id) {
        var i,
            length;
        length = $scope.guitarParts.doc.length;
        for (i = 0; i < length; i += 1) {
            if ($scope.guitarParts.doc[i]._id === id) {
                return $scope.guitarParts.doc[i];
            }
        }
        return -1;
    };

    $scope.changePartType = function (listindex) {
        var list,
            i;
        list = document.querySelectorAll("#builder-left-menu > li");
        for (i = 0; i < list.length; i++) {
            list[i].classList.remove("selected");
        }
        list[listindex].classList.add("selected");
        $scope.selectedPartType = list[listindex].innerHTML.toLowerCase();
        if ($scope.guitar.guitarParts[$scope.selectedPartType].material !== undefined) {
            $scope.material = $scope.guitar.guitarParts[$scope.selectedPartType].material;
        } else {
            $scope.guitar.guitarParts[$scope.selectedPartType].material = $scope.material;
        }
    };

    $scope.hexToRGB = function hexToRGB(hex) {
        var long = parseInt(hex.replace(/^#/, ""), 16);
        return {
            R: (long >>> 16) & 0xff,
            G: (long >>> 8) & 0xff,
            B: long & 0xff
        };
    };

    $scope.save = function (redirect, url) {
        if ($scope.guitar.name === undefined || $scope.guitar.name === null || $scope.guitar.name === "") {
            var nameInput = document.getElementById("builder-name");
            nameInput.classList.add("warning");
            return false;
        }
        if ($scope.guitar !== undefined && $scope.guitar._id !== undefined) {
            GuitarsService.guitars.update({ _id: $scope.guitar._id }, $scope.guitar, function (res) {
                var id;
                id = $scope.guitar._id;
                $scope.guitar = res.doc;
                $scope.guitar._id = id;
                console.log("ID: ");
                console.log(id);
                if (redirect) {
                    window.location = url;
                }
            });
            console.log($scope.guitar);
        } else {
            GuitarsService.guitars.save({}, $scope.guitar, function (res) {
                $scope.guitar = res.doc;
                $scope.guitar._id = res.doc._id;
                console.log("ID2: ");
                console.log(res.doc._id);
                console.log($scope.guitar._id);
                console.log($scope.guitar);
                if (redirect) {
                    window.location = url + $scope.guitar._id;
                }
            });
        }
        return true;
    };

    $scope.calculatePrice = function () {
        var properties,
            price,
            partId,
            part,
            i;
        price = 0.0;
        properties = ["body", "head", "neck", "frets", "pickup", "bridge", "buttons"];
        for (i = 0; i < properties.length; i += 1) {
            partId = $scope.guitar.guitarParts[properties[i]]._id;
            part = $scope.getGuitarPartById(partId);
            if (!isNaN(part.price)) {
                price += part.price;
            }
        }
        $scope.guitarPrice = price;
    };

    $scope.changeMaterial = function (next) {
        var materials,
            currentMaterialIndex;
        materials = ["/content/img/hout.jpg", "/content/img/plastic.png", "/content/img/steel.jpg"];
        currentMaterialIndex = materials.indexOf($scope.material);
        if (next) {
            currentMaterialIndex++;
        } else {
            currentMaterialIndex--;
        }
        if (currentMaterialIndex >= materials.length) {
            currentMaterialIndex = 0;
        } else if (currentMaterialIndex < 0) {
            currentMaterialIndex = materials.length - 1;
        }
        $scope.material = materials[currentMaterialIndex];
        $scope.guitar.guitarParts[$scope.selectedPartType].material = $scope.material;
    };

    $scope.order = function () {
        $scope.save(true, "/#/cart?guitarid=");
    };

    /* Facebook settings and functions*/
    $scope.loginStatus = 'disconnected';
    $scope.facebookIsReady = false;
    $scope.user = [];
    $scope.user.first_name = 'Login';
    $scope.login = homepage.login;

    $scope.removeAuth = homepage.removeAuth;

    $scope.api = homepage.api;
    $scope.share = function () {
        if ($scope.save()) {
            homepage.shareGuitar('http://server3.tezzt.nl:6075/#/builder/' + $scope.guitar._id);
        }
    };

    $scope.$watch(function () {
        return Facebook.isReady();
    }, function (newVal) {
        if (newVal) {
            $scope.facebookIsReady = true;
            Facebook.getLoginStatus(function (response) {
                $scope.loginStatus = response.status;
                if ($scope.loginStatus === 'connected') {
                    $scope.api();
                }
            });
        }
    }
        );

    var init = function () {
        $scope.guitarParts = GuitarsService.guitarParts.get();
        $scope.selectedPartType = "head";
        $scope.guitarPrice = 0;
        if ($scope.guitarParts.length > 0) {
            $scope.currentBodyImage = $scope.guitarParts[0].image;
        }
        if ($scope.guitar === undefined || $scope.guitar === null) {
            $scope.guitar = {
                type: "electric",
                userid: "eenId",
                likes: 0,
                dislike: 0,
                guitarParts: {
                    body: {
                    },
                    head: {
                    },
                    neck: {
                    },
                    frets: {
                    },
                    pickup: {
                    },
                    bridge: {
                    },
                    buttons: {
                    }
                }
            };
            $scope.material = "/content/img/hout.jpg";
        }
        if ($scope.color === undefined || $scope.color === null) {
            $scope.color = '#ff0000';
        }
        $scope.$watch('color', function (newVal, oldVal) {
            var rgbcode,
                canvas;
            rgbcode = $scope.hexToRGB(newVal);
            canvas = document.getElementById("cnvs" + $scope.selectedPartType);
            $scope.guitar.guitarParts[$scope.selectedPartType].color = $scope.color;
            $scope.changeColor(canvas, rgbcode);
        });
        $scope.$watch('guitarname', function (newVal, oldVal) {
            $scope.guitar.name = newVal;
            if (newVal !== "") {
                var nameInput = document.getElementById("builder-name");
                nameInput.classList.remove("warning");
            }
        });
    };
    init();
}]);