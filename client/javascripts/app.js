/*global angular, BookListCtrl, BookDetailCtrl */


/**
 *
 * Writing AngularJS Documentation
 *
 * @see https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation
 * @see http://docs.angularjs.org/guide/concepts
 */
var guitarDesignApp = angular.module('guitarDesignApp', ['guitarDesignApp.services', 'ngRoute', 'mp.colorPicker', 'facebook'])
    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        "use strict";

        $routeProvider.when('/guitars', {
            templateUrl: 'partials/guitars.html',
            controller: 'GuitarsController'
        });

        $routeProvider.when('/builder', {
            templateUrl: 'partials/builder.html',
            controller: 'BuilderController'
        });

        $routeProvider.when('/cart', {
            templateUrl: 'partials/cart.html',
            controller: 'CartController'
        });

        $routeProvider.when('/api/docs', {
            templateUrl: 'partials/documentation.html',
            controller: 'DocumentationController'
        });

        $routeProvider.when('/', {
            templateUrl: 'partials/home.html',
            controller: 'HomeController'
        });

        // When no valid route is provided
        $routeProvider.otherwise({
            redirectTo: "/"
        });
    }]);

