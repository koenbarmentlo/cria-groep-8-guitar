/*jslint node: true */
/*globals guitarDesignApp */

guitarDesignApp.controller('CartController', ['$scope', 'GuitarsService', function ($scope, GuitarsService) {
    "use strict";
    $scope.getUrlVars = function () {
        var vars = {};
        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    };

    $scope.getGuitar = function () {
        GuitarsService.guitars.get({ _id: $scope.guitarid }, function (data) {
            $scope.guitar = data.doc;
        });
    };

    $scope.calculatePrice = function () {
        var properties,
            price,
            partId,
            part,
            i;
        price = 0.0;
        properties = ["body", "head", "neck", "frets", "pickup", "bridge", "buttons"];
        for (i = 0; i < properties.length; i += 1) {
            try {
                partId = $scope.guitar.guitarParts[properties[i]]._id;
                part = $scope.getGuitarPartById(partId);
                if (!isNaN(part.price)) {
                    price += part.price;
                }
            } catch (ignore) {}
        }
        $scope.guitarPrice = price;
    };

    $scope.getGuitarPartById = function (id) {
        var i,
            length;
        length = $scope.guitarParts.doc.length;
        for (i = 0; i < length; i += 1) {
            if ($scope.guitarParts.doc[i]._id === id) {
                return $scope.guitarParts.doc[i];
            }
        }
        return -1;
    };
    $scope.$watch('guitar', function (newVal, oldVal) {
        $scope.calculatePrice();
    });
    $scope.$watch('guitarParts', function (newVal, oldVal) {
        $scope.calculatePrice();
    });

    $scope.guitarParts = GuitarsService.guitarParts.get();
    $scope.getGuitar();
    $scope.guitarid = $scope.getUrlVars().guitarid;
}]);